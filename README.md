# Online BookStore #

Design and implement (using JPA) the domain model for a system used by a bookstore that needs to sell its products online. These are the required features:
There are multiple categories of books (genres) - science fiction, horror, etc. A book can belong to multiple genres.
A book can be published by multiple publishing houses, and the price may vary based on the publisher.
A customer can search for books based on title, genre or author. Stock information must also be provided (in stock / out of stock).