package bookstore.app;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import bookstore.dao.AuthorDAO;
import bookstore.dao.AuthorDAOImpl;
import bookstore.model.Author;
import bookstore.model.Book;
import bookstore.model.Genre;
import bookstore.model.Publisher;

public class TestRun {

	public static void main(String[] args) {

		Publisher publisher = new Publisher("Polirom");
		Genre genre1 = new Genre("Horror");
		Genre genre2 = new Genre("Adventure");
		Genre genre3 = new Genre("Comedy");

		List<Genre> genres = new ArrayList<Genre>();
		genres.add(genre1);
		genres.add(genre2);
		genres.add(genre3);

		Author author1 = new Author("Ion Creanga");
		Author author2 = new Author("Eminescu");

		List<Author> authors = new ArrayList();
		authors.add(author1);
		authors.add(author2);

		Book book1 = new Book("JavaEE", genres, authors);

		List<Book> books = new ArrayList();
		books.add(book1);

		//EntityManagerFactory emf = Persistence.createEntityManagerFactory("bookstorePU");
		//AuthorDAO authorDAO = new AuthorDAOImpl(emf);
		//authorDAO.delete(1);
		//emf.close();
		
		//AuthorDAO authorDAO = new AuthorDAOImpl();
		//authorDAO.update(author1);
		//authorDAO.update(author2);

		//authorDAO.close();
	}

}
