package bookstore.model;

import javax.persistence.*;

@Entity
@Table(name = "PUBLISHERS")
public class Publisher {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	public Publisher(String name) {
		this.name = name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Publisher() {
		// TODO Auto-generated constructor stub
	}
}
