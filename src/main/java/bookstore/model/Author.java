package bookstore.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "AUTHORS")
public class Author {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	@ManyToMany
	@JoinTable(name = "books_authors", joinColumns = {
			@JoinColumn(name = "author_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "book_id", referencedColumnName = "id") })
	private List<Book> books;

	public Author(String name) {
		this.name = name;
	}

	public Author(String name, List<Book> books) {
		this.name = name;
		this.books = books;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public Author() {
		// TODO Auto-generated constructor stub
	}
}
