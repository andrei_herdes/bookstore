package bookstore.model;

import java.time.LocalDateTime;

import javax.persistence.*;

@Entity
@Table(name = "EDITIONS")
public class Edition {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	private Book book;
	@ManyToOne
	private Publisher publisher;

	private double price;
	private LocalDateTime pubDate;

	public Edition(Book book, Publisher publisher, double price, LocalDateTime pubDate) {
		this.book = book;
		this.publisher = publisher;
		this.price = price;
		this.pubDate = pubDate;
	}

	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public LocalDateTime getPubDate() {
		return pubDate;
	}

	public void setPubDate(LocalDateTime pubDate) {
		this.pubDate = pubDate;
	}

	public Edition() {
	}

}
