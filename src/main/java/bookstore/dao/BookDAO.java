package bookstore.dao;

import bookstore.model.Author;
import bookstore.model.Book;

public interface BookDAO {

	public Book findById(long id);

	public void delete(long id);

	public Book update(Book book);

	public void close();
	
}
