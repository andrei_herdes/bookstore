package bookstore.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import bookstore.model.Book;

public class BookDAOImpl implements BookDAO {

	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("bookstorePU");
	private EntityManager em;

	public BookDAOImpl() {
		em = emf.createEntityManager();

	}

	@Override
	public Book findById(long id) {
		Book book = null;
		try {
			em.getTransaction().begin();
			book = em.find(Book.class, id);
			em.getTransaction().commit();
			return book;
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		}
		return book;
	}

	@Override
	public void delete(long id) {
		try {
			em.getTransaction().begin();
			Book book = em.find(Book.class, id);
			em.remove(book);
			em.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		}

	}

	@Override
	public Book update(Book book) {
		Book b = null;
		try {
			em.getTransaction().begin();
			b = em.merge(book);
			em.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		}
		return b;
	}
	
	public void close() {
		emf.close();
	}


	
}
