package bookstore.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import bookstore.model.Author;

public class AuthorDAOImpl implements AuthorDAO {

	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("bookstorePU");
	private EntityManager em;

	public AuthorDAOImpl() {
		em = emf.createEntityManager();
	}

	@Override
	public Author findById(long id) {
		Author author = null;
		try {
			em.getTransaction().begin();
			author = em.find(Author.class, id);
			em.getTransaction().commit();
			return author;
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		}
		return author;
	}

	@Override
	public void delete(long id) {
		try {
			em.getTransaction().begin();
			Author author = em.find(Author.class, id);
			em.remove(author);
			em.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		}

	}

	@Override
	public Author update(Author author) {

		Author a = null;
		try {
			em.getTransaction().begin();
			a = em.merge(author);
			em.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		}
		return a;
	}
	
	@Override
	public void close() {
		emf.close();
	}

}
