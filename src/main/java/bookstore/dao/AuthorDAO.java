package bookstore.dao;

import bookstore.model.Author;

public interface AuthorDAO {

	public Author findById(long id);

	public void delete(long id);

	public Author update(Author author);
	
	public void close();

}
